#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/image_transformer.h"



uint8_t* read_image(const char* filename) {
    unsigned int res = 0;
    size_t size = 0;
    if (!filename) {
        return NULL;
    }

    FILE* fin = fopen(filename, "rb");
    if (!fin) {
        return NULL;
    }

    fseek(fin, 0, SEEK_END);
    size = ftell(fin);
    fseek(fin, 0, SEEK_SET);

    uint8_t* buf = calloc(size, 1);
    if (!buf) {
        fclose(fin);
        return NULL;
    }

    res = fread(buf, size, 1, fin);
    if (res < 0) {
        free(buf);
        fclose(fin);
        return NULL;
    }

    fclose(fin);
    return buf;
}


uint8_t*  turn_image(uint8_t*  buffer) {
    uint8_t*  output_buffer = NULL;
    bmp_header_t* header = (bmp_header_t*) buffer;
    bmp_info_header_t* header_info = (bmp_info_header_t*) (buffer + sizeof(bmp_header_t));
    size_t width = header_info->width, height = header_info->height;

    int32_t temp = header_info->hres;
    header_info->height = width;
    header_info->width = height;
    header_info->hres = header_info->vres;
    header_info->vres = temp;

    size_t real_output_width = (height * 3) % 4 ? ((height * 3) / 4 + 1) * 4 : height * 3;
    header->file_size = 54 + real_output_width * width;

    output_buffer = calloc(header->file_size, 1);

    for (int i = 0; i < header->offset; ++i) {
        output_buffer[i] = buffer[i];
    }

    uint8_t* input = buffer + header->offset;
    uint8_t* output = output_buffer + header->offset;

    FILE* fout = fopen("outputdump.bmp", "wb");
    fwrite(buffer, header->offset, 1, fout);

    uint32_t real_inp_width = (width * 3) % 4 ? ((width * 3) / 4 + 1) * 4 : (width * 3);
    size_t offset = (height * 3) % 4 ? 4 - (height * 3) % 4 : 0;
    size_t counter = 0;

    for (size_t x = 0; x < width; ++x) {
        for (size_t y = height - 1; y < height; --y) {
            output[counter] = input[x * 3 + real_inp_width * y];
            ++counter;
            output[counter] = input[x * 3 + real_inp_width * y + 1];
            ++counter;
            output[counter] = input[x * 3 + real_inp_width * y + 2];
            ++counter;
        }

        for (size_t k = 0; k < offset; ++k) {
            output[counter] = 0;
            ++counter;
        }
    }

    return output_buffer;
}


int write_image(uint8_t*  buffer, const char* filename) {
    FILE* fout = fopen(filename, "wb");
    if (!fout) {
        printf("File %s opening failed!\n", filename);
        return -1;
    }
    bmp_header_t* hdr = (bmp_header_t*) buffer;

    fwrite(buffer, hdr->file_size, 1, fout);
    fclose(fout);
    return 0;
}
