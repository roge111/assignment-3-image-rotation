#include <stdio.h>
#include <stdlib.h>

#include "../include/image_transformer.h"


int main(int argc, char** argv) {
    if (argc != 3) {
        printf("usage: ./image_transformer <input_file> <output_file>\n");
        return 1;
    }
    void* input_buf, *output_buf = NULL;

    input_buf = read_image(argv[1]);
    if (!input_buf) {
        printf("Reading from file failed!\n");
        return 1;
    }

    output_buf = turn_image(input_buf);
    if (!output_buf) {
        printf("Turning the image failed!\n");
        free(input_buf);
        return 1;
    }

    if (write_image(output_buf, argv[2]) < 0) {
        printf("Writing to file %s failed!\n", argv[2]);
    }

    free(input_buf);
    free(output_buf);

    return 0;
}
