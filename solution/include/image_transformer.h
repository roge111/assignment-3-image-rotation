#ifndef IMAGE_TRANSFORMER_H
#define IMAGE_TRANSFORMER_H

#include <stdint.h>


#pragma pack(push, 1)
typedef struct bmp_header_s {
    uint8_t  bmp_header[2];
    uint32_t file_size;
    uint16_t rsvd[2];
    uint32_t offset;
} bmp_header_t;
#pragma pack(pop)


#pragma pack(push, 1)
typedef struct bmp_info_header_s {
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t color_planes;
    uint16_t bits_per_pixel;
    uint32_t compression_method;
    uint32_t image_size;
    int32_t  hres;
    int32_t  vres;
    uint32_t colors_num;
    uint32_t important_colors;
} bmp_info_header_t;
#pragma pack(pop)


uint8_t*  read_image(const char* filename);
uint8_t*  turn_image(uint8_t*  buffer);
int write_image(uint8_t*  buffer, const char* filename);

#endif
